<?php
session_start();
include('db.php');
if (isset($_SESSION['user_uname']) == "") {
    header("Location: login.php");
}

$status = "";
if (isset($_POST['id']) && $_POST['id'] != "") {
    $id = $_POST['id'];
    $result = mysqli_query($conn, "SELECT * FROM products WHERE id_product='" . $id . "'");
    $row = mysqli_fetch_assoc($result);
    $name = $row['name'];
    $id = $row['id_product'];
    $price = $row['price'];
    $image = $row['image'];

    $cartArray = array(
        $id => array(
            'name' => $name,
            'id' => $id,
            'price' => $price,
            'quantity' => 1,
            'image' => $image
        )
    );

    if (empty($_SESSION["shopping_cart"])) {
        $_SESSION["shopping_cart"] = $cartArray;
        $status = "<div class='box'>Product is added to your cart!</div>";
    } else {
        $array_keys = array_keys($_SESSION["shopping_cart"]);
        if (in_array($id, $array_keys)) {
            $status = "<div class='box' style='color:red;'>Product is already added to your cart!</div>";
        } else {
            $_SESSION["shopping_cart"] = array_merge($_SESSION["shopping_cart"], $cartArray);
            $status = "<div class='box'>Product is added to your cart!</div>";
        }
    }
}
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Shopping Cart</title>
    <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
</head>

<body>

    <div style="width:700px; margin:50 auto;">
        <h2>Welcome page, <?php echo $_SESSION['user_uname'] . "!" ?></h2>
        <a href="logout.php">Logout</a>
        <br>
        <?php
        if (!empty($_SESSION["shopping_cart"])) {
            $cart_count = count(array_keys($_SESSION["shopping_cart"]));
            echo '<div class="cart_div">
                <a href="cart.php"><img src="cart-icon.png" /> Cart<span>' . $cart_count . '</span></a>
                </div><br><br>';
        }
        ?>

        <?php
        $result = mysqli_query($conn, "SELECT * FROM products");
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<div class='product_wrapper'>
			                <form method='post' action=''>
			                <input type='hidden' name='id' value=" . $row['id_product'] . " />
			                <div class='image'><img src='" . $row['image'] . "' /></div>
			                <div class='name'>" . $row['name'] . "</div>
		   	                <div class='price'>$" . $row['price'] . "</div>
			                <button type='submit' class='buy'>Buy Now</button>
			                </form>
		   	                </div>";
        }
        mysqli_close($conn);
        ?>

        <div style="clear:both;"></div>
        <div class="message_box" style="margin:10px 0px;">
            <?php echo $status; ?>
        </div>

    </div>

</body>

</html>