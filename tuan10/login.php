<?php
session_start();
require_once "db.php";
if (isset($_SESSION['user_uname']) != ""){
    header("Location: home.php");
}

if (isset($_POST['login'])) {
    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    if (!preg_match("/^[a-zA-Z0-9]$/", $username)) {
        $username_error = "Username is only character and number";
    }
    if (strlen($password) < 6) {
        $password_error = "Password must be minimum of 6 characters";
    }
    $result = mysqli_query($conn, "SELECT * FROM Users WHERE UserName = '" . $username . "' and Password = '" . $password . "'");
    if (!empty($result)) {
        if ($row = mysqli_fetch_array($result)) {
            $_SESSION['user_name'] = $row['DisplayName'];
            $_SESSION['user_uname'] = $row['UserName'];
            $_SESSION['user_email'] = $row['Email'];
            $_SESSION['user_phone'] = $row['PhoneNumber'];
            header("Location: home.php");
        }
    } else {
        $error_message = "Incorrect Username or Password!!!";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-10">
                <div class="page-header">
                    <h2>Login</h2>
                </div>
                <p>Please fill all fields in the form</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group ">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" value="" maxlength="20" required="">
                        <span class="text-danger"><?php if (isset($username_error)) echo $username_error; ?></span>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" value="" maxlength="8" required="">
                        <span class="text-danger"><?php if (isset($password_error)) echo $password_error; ?></span>
                    </div>
                    <input type="submit" class="btn btn-primary" name="login" value="submit">
                    <br>
                    You don't have account? <a href="registration.php" class="mt-3">Click Here</a>
                </form>
            </div>
        </div>
    </div>
</body>

</html>