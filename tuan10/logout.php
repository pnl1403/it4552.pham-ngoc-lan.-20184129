<?php
ob_start();
session_start();
if (isset($_SESSION['user_uname'])) {
    session_destroy();
    unset($_SESSION['user_name']);
    unset($_SESSION['user_uname']);
    unset($_SESSION['user_email']);
    unset($_SESSION['user_phone']);
    header("Location: login.php");
} else {
    header("Location: login.php");
}
?>
